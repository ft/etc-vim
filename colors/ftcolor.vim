""" vim:ft=vim:fdm=marker

set background=dark
if version > 580
    hi clear
    if exists("syntax_on")
        syntax reset
    endif
endif
let g:colors_name="ftcolor"

if has("gui_running")
"{{{

    hi Comment                      gui=NONE        guifg=darkyellow        guibg=NONE
    hi Constant                     gui=NONE        guifg=#FF3030           guibg=NONE
    hi Cursor                       gui=NONE        guifg=#000000           guibg=#00FF00
    hi CursorColumn                 gui=NONE        guifg=NONE              guibg=grey25
    hi CursorLine                   gui=NONE        guifg=NONE              guibg=grey25

    hi DiffAdd                      gui=NONE        guifg=NONE              guibg=lightblue
    hi DiffChange                   gui=NONE        guifg=NONE              guibg=darkmagenta
    hi DiffDelete                   gui=bold        guifg=lightblue         guibg=cyan
    hi DiffText                     gui=bold        guifg=NONE              guibg=red
    hi Directory                    gui=NONE        guifg=darkcyan          guibg=NONE
    hi DoxygenArgumentWord          gui=NONE        guifg=lightblue         guibg=bg
    hi DoxygenBriefL                gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenBriefLine             gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenBOther                gui=NONE        guifg=#00ff00           guibg=bg
    hi DoxygenComment               gui=NONE        guifg=lightyellow       guibg=NONE
    hi DoxygenCommentL              gui=NONE        guifg=lightgreen        guibg=NONE
    hi DoxygenParam                 gui=NONE        guifg=#00ff00           guibg=bg
    hi DoxygenParamName             gui=NONE        guifg=lightblue         guibg=bg
    hi DoxygenParamDirection        gui=NONE        guifg=#ffff00           guibg=bg
    hi DoxygenSmallSpecial          gui=NONE        guifg=#00ff00           guibg=bg
    hi DoxygenSpecial               gui=NONE        guifg=#00ff00           guibg=bg
    hi DoxygenSpecialOneLineDesc    gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenSpecialMultiLineDesc  gui=NONE        guifg=#00aa00           guibg=bg
    hi DoxygenStart                 gui=NONE        guifg=lightyellow       guibg=NONE
    hi DoxygenStartL                gui=NONE        guifg=#00ff00           guibg=NONE

    hi Error                        gui=NONE        guifg=grey              guibg=red
    hi ErrorMsg                     gui=NONE        guifg=grey              guibg=red

    hi Folded                       gui=NONE        guifg=#2244FF           guibg=NONE
    hi FoldColumn                   gui=NONE        guifg=brown             guibg=NONE

    hi Identifier                   gui=NONE        guifg=cyan              guibg=NONE
    hi Ignore                       gui=NONE        guifg=blue              guibg=NONE
    hi IncSearch                    gui=NONE        guifg=yellow            guibg=green

    hi LineNr                       gui=NONE        guifg=yellow            guibg=NONE
    hi LineTooLong                  gui=NONE        guifg=green             guibg=black

    hi MatchParen                   gui=bold        guifg=darkmagenta       guibg=black
    hi MoreMsg                      gui=NONE        guifg=darkgreen         guibg=NONE
    hi ModeMsg                      gui=bold        guifg=#88FFEE           guibg=grey35

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      gui=NONE        guifg=white             guibg=red
    hi Normal                       gui=NONE        guifg=grey80            guibg=grey10

    hi PreProc                      gui=NONE        guifg=#CC22CC           guibg=NONE
    hi Pmenu                        gui=NONE        guifg=#88FFEE           guibg=grey35
    hi PmenuSel                     gui=NONE        guifg=grey35            guibg=#88FFEE
    hi PmenuSbar                    gui=NONE        guifg=#88FFEE           guibg=#88FFEE
    hi PmenuThumb                   gui=NONE        guifg=black             guibg=black

    hi Question                     gui=NONE        guifg=green             guibg=NONE

    hi Search                       gui=NONE        guifg=grey              guibg=blue
    hi Special                      gui=NONE        guifg=#CC22CC           guibg=NONE
    hi SpecialKey                   gui=NONE        guifg=#00FF00           guibg=NONE
    hi Statement                    gui=NONE        guifg=cyan              guibg=NONE
    hi StatusLine                   gui=NONE        guifg=skyblue           guibg=black
    hi StatusLineNC                 gui=NONE        guifg=grey              guibg=NONE

    hi TabLine                      gui=NONE        guifg=#2244AA           guibg=black
    hi TabLineSel                   gui=bold        guifg=green             guibg=black
    hi TabLinePrev                  gui=NONE        guifg=darkmagenta       guibg=black
    hi TabModded                    gui=bold        guifg=white             guibg=red
    hi TabLineFill                  gui=NONE        guifg=black             guibg=black
    hi TabSeparator                 gui=bold        guifg=#0000FF           guibg=black
    hi Title                        gui=NONE        guifg=darkmagenta       guibg=NONE
    hi Type                         gui=NONE        guifg=green             guibg=NONE

    hi Underlined                   gui=underline   guifg=#CC22CC           guibg=NONE

    hi VertSplit                    gui=reverse     guifg=NONE              guibg=NONE
    hi Visual                       gui=NONE        guibg=#5588AA           guifg=#CCFFCC
    hi VisualNOS                    gui=bold        guibg=NONE              guifg=NONE

    hi WarningMsg                   gui=NONE        guifg=grey              guibg=red
    hi WildMenu                     gui=NONE        guifg=black             guibg=darkyellow

"}}}
elseif &t_Co > 16
"{{{

    hi Comment                      cterm=NONE      ctermfg=3               ctermbg=NONE
    hi Constant                     cterm=NONE      ctermfg=red             ctermbg=NONE
    hi Cursornt                     cterm=NONE      ctermfg=NONE            ctermbg=NONE
    hi CursorLine                   cterm=underline ctermfg=NONE            ctermbg=NONE
    hi CursorColumn                 cterm=NONE      ctermfg=NONE            ctermbg=grey

    hi DoxygenArgumentWord          cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi Directory                    cterm=NONE      ctermfg=darkcyan        ctermbg=NONE
    hi DiffAdd                      cterm=NONE      ctermfg=NONE            ctermbg=lightblue
    hi DiffChange                   cterm=NONE      ctermfg=NONE            ctermbg=darkmagenta
    hi DiffDelete                   cterm=bold      ctermfg=lightblue       ctermbg=cyan
    hi DiffText                     cterm=bold      ctermfg=NONE            ctermbg=red
    hi DoxygenBOther                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenBriefLine             cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenBriefL                cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenComment               cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenCommentL              cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenParam                 cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenParamDirection        cterm=NONE      ctermfg=yellow          ctermbg=NONE
    hi DoxygenParamName             cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi DoxygenSmallSpecial          cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenSpecialOneLineDesc    cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecialMultiLineDesc  cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecial               cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenStart                 cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenStartL                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE

    hi Error                        cterm=bold      ctermfg=grey            ctermbg=red
    hi ErrorMsg                     cterm=bold      ctermfg=grey            ctermbg=red

    hi Folded                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi FoldColumn                   cterm=NONE      ctermfg=darkyellow      ctermbg=NONE

    hi Identifier                   cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi Ignore                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi IncSearch                    cterm=NONE      ctermfg=yellow          ctermbg=green

    hi LineNr                       cterm=NONE      ctermfg=darkyellow      ctermbg=NONE
    hi LineTooLong                  cterm=bold      ctermfg=white           ctermbg=blue

    hi MatchParen                   cterm=bold      ctermfg=darkmagenta     ctermbg=black
    hi MoreMsg                      cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi ModeMsg                      cterm=bold      ctermfg=green           ctermbg=NONE

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      cterm=bold      ctermfg=white           ctermbg=red
    hi Normal                       cterm=NONE      ctermfg=NONE            ctermbg=NONE

    hi Pmenu                        cterm=NONE      ctermfg=white           ctermbg=blue
    hi PmenuSbar                    cterm=NONE      ctermbg=white           ctermfg=white
    hi PmenuSel                     cterm=NONE      ctermfg=blue            ctermbg=white
    hi PmenuThumb                   cterm=NONE      ctermfg=black           ctermbg=black
    hi PreProc                      cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE

    hi Question                     cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Search                       cterm=NONE      ctermfg=grey            ctermbg=blue
    hi Special                      cterm=NONE      ctermfg=magenta         ctermbg=NONE
    hi SpecialKey                   cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi Statement                    cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi StatusLine                   cterm=NONE      ctermfg=blue            ctermbg=black
    hi StatusLineNC                 cterm=NONE      ctermfg=grey            ctermbg=NONE

    hi TabLine                      cterm=NONE      ctermfg=blue            ctermbg=black
    hi TabLineFill                  cterm=NONE      ctermfg=black           ctermbg=black
    hi TabLinePrev                  cterm=NONE      ctermfg=darkmagenta     ctermbg=black
    hi TabLineSel                   cterm=bold      ctermfg=green           ctermbg=black
    hi TabModded                    cterm=bold      ctermfg=white           ctermbg=red
    hi TabSeparator                 cterm=bold      ctermfg=blue            ctermbg=black
    hi Title                        cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE
    hi Type                         cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Underlined                   cterm=underline ctermfg=magenta         ctermbg=NONE

    hi VertSplit                    cterm=reverse   ctermfg=NONE            ctermbg=NONE
    hi Visual                       cterm=NONE      ctermbg=blue            ctermfg=white
    hi VisualNOS                    cterm=bold      ctermbg=NONE            ctermfg=NONE

    hi WarningMsg                   cterm=bold      ctermfg=grey            ctermbg=red
    hi WildMenu                     cterm=NONE      ctermfg=black           ctermbg=darkyellow

" }}}
elseif &t_Co == 8 || &t_Co == 16
"{{{

    hi Comment                      cterm=NONE      ctermfg=3               ctermbg=NONE
    hi Constant                     cterm=NONE      ctermfg=red             ctermbg=NONE
    hi Cursornt                     cterm=NONE      ctermfg=NONE            ctermbg=NONE
    hi CursorLine                   cterm=underline ctermfg=NONE            ctermbg=NONE
    hi CursorColumn                 cterm=NONE      ctermfg=NONE            ctermbg=grey

    hi DoxygenArgumentWord          cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi Directory                    cterm=NONE      ctermfg=darkcyan        ctermbg=NONE
    hi DiffAdd                      cterm=NONE      ctermfg=NONE            ctermbg=lightblue
    hi DiffChange                   cterm=NONE      ctermfg=NONE            ctermbg=darkmagenta
    hi DiffDelete                   cterm=bold      ctermfg=lightblue       ctermbg=cyan
    hi DiffText                     cterm=bold      ctermfg=NONE            ctermbg=red
    hi DoxygenBOther                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenBriefLine             cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenBriefL                cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenComment               cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenCommentL              cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenParam                 cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenParamDirection        cterm=NONE      ctermfg=yellow          ctermbg=NONE
    hi DoxygenParamName             cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi DoxygenSmallSpecial          cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenSpecialOneLineDesc    cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecialMultiLineDesc  cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecial               cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenStart                 cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenStartL                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE

    hi Error                        cterm=bold      ctermfg=grey            ctermbg=red
    hi ErrorMsg                     cterm=bold      ctermfg=grey            ctermbg=red

    hi Folded                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi FoldColumn                   cterm=NONE      ctermfg=darkyellow      ctermbg=NONE

    hi Identifier                   cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi Ignore                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi IncSearch                    cterm=NONE      ctermfg=yellow          ctermbg=green

    hi LineNr                       cterm=NONE      ctermfg=darkyellow      ctermbg=NONE
    hi LineTooLong                  cterm=bold      ctermfg=white           ctermbg=blue

    hi MatchParen                   cterm=bold      ctermfg=darkmagenta     ctermbg=black
    hi MoreMsg                      cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi ModeMsg                      cterm=bold      ctermfg=green           ctermbg=NONE

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      cterm=bold      ctermfg=white           ctermbg=red
    hi Normal                       cterm=NONE      ctermfg=NONE            ctermbg=NONE

    hi Pmenu                        cterm=NONE      ctermfg=white           ctermbg=blue
    hi PmenuSbar                    cterm=NONE      ctermbg=white           ctermfg=white
    hi PmenuSel                     cterm=NONE      ctermfg=blue            ctermbg=white
    hi PmenuThumb                   cterm=NONE      ctermfg=black           ctermbg=black
    hi PreProc                      cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE

    hi Question                     cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Search                       cterm=NONE      ctermfg=grey            ctermbg=blue
    hi Special                      cterm=NONE      ctermfg=magenta         ctermbg=NONE
    hi SpecialKey                   cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi Statement                    cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi StatusLine                   cterm=NONE      ctermfg=blue            ctermbg=black
    hi StatusLineNC                 cterm=NONE      ctermfg=grey            ctermbg=NONE

    hi TabLine                      cterm=NONE      ctermfg=blue            ctermbg=black
    hi TabLineFill                  cterm=NONE      ctermfg=black           ctermbg=black
    hi TabLinePrev                  cterm=NONE      ctermfg=darkmagenta     ctermbg=black
    hi TabLineSel                   cterm=bold      ctermfg=green           ctermbg=black
    hi TabModded                    cterm=bold      ctermfg=white           ctermbg=red
    hi TabSeparator                 cterm=bold      ctermfg=blue            ctermbg=black
    hi Title                        cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE
    hi Type                         cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Underlined                   cterm=underline ctermfg=magenta         ctermbg=NONE

    hi VertSplit                    cterm=reverse   ctermfg=NONE            ctermbg=NONE
    hi Visual                       cterm=NONE      ctermbg=blue            ctermfg=white
    hi VisualNOS                    cterm=bold      ctermbg=NONE            ctermfg=NONE

    hi WarningMsg                   cterm=bold      ctermfg=grey            ctermbg=red
    hi WildMenu                     cterm=NONE      ctermfg=black           ctermbg=darkyellow

" }}}
endif
