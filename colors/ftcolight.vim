"" vim:ft=vim:fdm=marker
" A colorscheme that works on light backgrounds and doesn't suck. :)

set background=light
hi clear
if version > 580
    hi clear
    if exists("syntax_on")
        syntax reset
    endif
endif
let g:colors_name = "ftcolight"

if has("gui_running")
"{{{

    hi Boolean                      gui=NONE        guifg=red               guibg=NONE

    hi Character                    gui=NONE        guifg=red               guibg=NONE
    hi Comment                      gui=NONE        guifg=#4477AA           guibg=NONE
    hi Conditional                  gui=NONE        guifg=brown             guibg=NONE
    hi Constant                     gui=NONE        guifg=red               guibg=NONE
    hi Cursor                       gui=NONE        guifg=#DDDDDD           guibg=#FF0000
    hi CursorColumn                 gui=NONE        guifg=NONE              guibg=grey65
    hi CursorLine                   gui=NONE        guifg=NONE              guibg=grey65

    hi Debug                        gui=NONE        guifg=magenta           guibg=NONE
    hi Define                       gui=NONE        guifg=purple            guibg=NONE
    hi Delimiter                    gui=NONE        guifg=magenta           guibg=NONE
    hi DiffAdd                      gui=NONE        guifg=NONE              guibg=lightblue
    hi DiffChange                   gui=NONE        guifg=NONE              guibg=darkmagenta
    hi DiffDelete                   gui=bold        guifg=lightblue         guibg=cyan
    hi DiffText                     gui=bold        guifg=NONE              guibg=red
    hi Directory                    gui=NONE        guifg=darkcyan          guibg=NONE
    hi DoxygenArgumentWord          gui=NONE        guifg=lightblue         guibg=NONE
    hi DoxygenBriefL                gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenBriefLine             gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenBOther                gui=NONE        guifg=red               guibg=NONE
    hi DoxygenComment               gui=NONE        guifg=grey15            guibg=NONE
    hi DoxygenCommentL              gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenParam                 gui=NONE        guifg=red               guibg=NONE
    hi DoxygenParamName             gui=NONE        guifg=darkyellow        guibg=NONE
    hi DoxygenParamDirection        gui=NONE        guifg=#cccc00           guibg=NONE
    hi DoxygenSmallSpecial          gui=NONE        guifg=#00ff00           guibg=NONE
    hi DoxygenSpecial               gui=NONE        guifg=red               guibg=NONE
    hi DoxygenSpecialOneLineDesc    gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenSpecialMultiLineDesc  gui=NONE        guifg=#00aa00           guibg=NONE
    hi DoxygenStart                 gui=NONE        guifg=grey15            guibg=NONE
    hi DoxygenStartL                gui=NONE        guifg=grey15            guibg=NONE

    hi Error                        gui=NONE        guifg=white             guibg=red
    hi ErrorMsg                     gui=NONE        guifg=white             guibg=red
    hi Exception                    gui=NONE        guifg=brown             guibg=NONE

    hi Float                        gui=NONE        guifg=red               guibg=NONE
    hi Folded                       gui=NONE        guifg=#2244FF           guibg=NONE
    hi FoldColumn                   gui=NONE        guifg=brown             guibg=NONE
    hi Function                     gui=NONE        guifg=blue              guibg=NONE

    hi Identifier                   gui=NONE        guifg=blue              guibg=NONE
    hi Ignore                       gui=NONE        guifg=blue              guibg=NONE
    hi Include                      gui=NONE        guifg=purple            guibg=NONE
    hi IncSearch                    gui=NONE        guifg=yellow            guibg=green

    hi Keyword                      gui=NONE        guifg=brown             guibg=NONE

    hi Label                        gui=NONE        guifg=brown             guibg=NONE
    hi LineNr                       gui=NONE        guifg=yellow            guibg=NONE
    hi LineTooLong                  gui=NONE        guifg=green             guibg=black

    hi Macro                        gui=NONE        guifg=purple            guibg=NONE
    hi MatchParen                   gui=bold        guifg=darkmagenta       guibg=black
    hi MoreMsg                      gui=NONE        guifg=#4477AA           guibg=NONE
    hi ModeMsg                      gui=bold        guifg=#88FFEE           guibg=grey35

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      gui=NONE        guifg=white             guibg=red
    hi Normal                       gui=NONE        guifg=black             guibg=#ECF0EC
    hi Number                       gui=NONE        guifg=red               guibg=NONE

    hi Operator                     gui=NONE        guifg=brown             guibg=NONE

    hi PreCondit                    gui=NONE        guifg=purple            guibg=NONE
    hi PreProc                      gui=NONE        guifg=purple            guibg=NONE
    hi Pmenu                        gui=NONE        guifg=#88FFEE           guibg=grey35
    hi PmenuSel                     gui=NONE        guifg=blue              guibg=#88FFEE
    hi PmenuSbar                    gui=NONE        guifg=#448877           guibg=#448877
    hi PmenuThumb                   gui=NONE        guifg=black             guibg=black

    hi Question                     gui=NONE        guifg=#4477AA           guibg=NONE

    hi Repeat                       gui=NONE        guifg=brown             guibg=NONE

    hi Search                       gui=NONE        guifg=black             guibg=cyan
    hi Special                      gui=NONE        guifg=magenta           guibg=NONE
    hi SpecialComment               gui=NONE        guifg=magenta           guibg=NONE
    hi SpecialChar                  gui=NONE        guifg=magenta           guibg=NONE
    hi SpecialKey                   gui=NONE        guifg=#00FF00           guibg=NONE
    hi Statement                    gui=NONE        guifg=brown             guibg=NONE
    hi StatusLine                   gui=NONE        guifg=skyblue           guibg=black
    hi StatusLineNC                 gui=NONE        guifg=grey              guibg=NONE
    hi StorageClass                 gui=NONE        guifg=blue              guibg=NONE
    hi String                       gui=NONE        guifg=red               guibg=NONE
    hi Structure                    gui=NONE        guifg=blue              guibg=NONE

    hi TabLine                      gui=NONE        guifg=black             guibg=grey65
    hi TabLineSel                   gui=bold        guifg=#7744FF           guibg=#ECF0EC
    hi TabLinePrev                  gui=underline   guifg=grey65            guibg=grey35
    hi TabModded                    gui=bold        guifg=white             guibg=red
    hi TabLineFill                  gui=NONE        guifg=black             guibg=black
    hi TabSeparator                 gui=bold        guifg=#0000FF           guibg=black
    hi Tag                          gui=NONE        guifg=darkGreen         guibg=NONE
    hi Title                        gui=NONE        guifg=darkmagenta       guibg=NONE
    hi Todo                         gui=NONE        guifg=blue              guibg=yellow
    hi Type                         gui=NONE        guifg=blue              guibg=NONE
    hi Typedef                      gui=NONE        guifg=blue              guibg=NONE

    hi Underlined                   gui=underline   guifg=#CC22CC           guibg=NONE

    hi VertSplit                    gui=reverse     guifg=NONE              guibg=NONE
    hi Visual                       gui=NONE        guifg=black             guibg=yellow
    hi VisualNOS                    gui=bold        guifg=NONE              guibg=NONE

    hi WarningMsg                   gui=NONE        guifg=black             guibg=yellow
    hi WildMenu                     gui=NONE        guifg=black             guibg=darkyellow

"}}}
elseif &t_Co > 16
"{{{

    hi Comment                      cterm=NONE      ctermfg=3               ctermbg=NONE
    hi Constant                     cterm=NONE      ctermfg=red             ctermbg=NONE
    hi Cursornt                     cterm=NONE      ctermfg=NONE            ctermbg=NONE
    hi CursorLine                   cterm=underline ctermfg=NONE            ctermbg=NONE
    hi CursorColumn                 cterm=NONE      ctermfg=NONE            ctermbg=grey

    hi DoxygenArgumentWord          cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi Directory                    cterm=NONE      ctermfg=darkcyan        ctermbg=NONE
    hi DiffAdd                      cterm=NONE      ctermfg=NONE            ctermbg=lightblue
    hi DiffChange                   cterm=NONE      ctermfg=NONE            ctermbg=darkmagenta
    hi DiffDelete                   cterm=bold      ctermfg=lightblue       ctermbg=cyan
    hi DiffText                     cterm=bold      ctermfg=NONE            ctermbg=red
    hi DoxygenBOther                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenBriefLine             cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenBriefL                cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenComment               cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenCommentL              cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenParam                 cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenParamDirection        cterm=NONE      ctermfg=yellow          ctermbg=NONE
    hi DoxygenParamName             cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi DoxygenSmallSpecial          cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenSpecialOneLineDesc    cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecialMultiLineDesc  cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecial               cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenStart                 cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenStartL                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE

    hi Error                        cterm=bold      ctermfg=grey            ctermbg=red
    hi ErrorMsg                     cterm=bold      ctermfg=grey            ctermbg=red

    hi Folded                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi FoldColumn                   cterm=NONE      ctermfg=darkyellow      ctermbg=NONE

    hi Identifier                   cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi Ignore                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi IncSearch                    cterm=NONE      ctermfg=yellow          ctermbg=green

    hi LineNr                       cterm=NONE      ctermfg=darkyellow      ctermbg=NONE
    hi LineTooLong                  cterm=bold      ctermfg=white           ctermbg=blue

    hi MatchParen                   cterm=bold      ctermfg=darkmagenta     ctermbg=black
    hi MoreMsg                      cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi ModeMsg                      cterm=bold      ctermfg=green           ctermbg=NONE

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      cterm=bold      ctermfg=white           ctermbg=red
    hi Normal                       cterm=NONE      ctermfg=NONE            ctermbg=NONE

    hi Pmenu                        cterm=NONE      ctermfg=white           ctermbg=blue
    hi PmenuSbar                    cterm=NONE      ctermbg=white           ctermfg=white
    hi PmenuSel                     cterm=NONE      ctermfg=blue            ctermbg=white
    hi PmenuThumb                   cterm=NONE      ctermfg=black           ctermbg=black
    hi PreProc                      cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE

    hi Question                     cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Search                       cterm=NONE      ctermfg=grey            ctermbg=blue
    hi Special                      cterm=NONE      ctermfg=magenta         ctermbg=NONE
    hi SpecialKey                   cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi Statement                    cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi StatusLine                   cterm=NONE      ctermfg=blue            ctermbg=black
    hi StatusLineNC                 cterm=NONE      ctermfg=grey            ctermbg=NONE

    hi TabLine                      cterm=NONE      ctermfg=blue            ctermbg=black
    hi TabLineFill                  cterm=NONE      ctermfg=black           ctermbg=black
    hi TabLinePrev                  cterm=NONE      ctermfg=darkmagenta     ctermbg=black
    hi TabLineSel                   cterm=bold      ctermfg=green           ctermbg=black
    hi TabModded                    cterm=bold      ctermfg=white           ctermbg=red
    hi TabSeparator                 cterm=bold      ctermfg=blue            ctermbg=black
    hi Title                        cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE
    hi Type                         cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Underlined                   cterm=underline ctermfg=magenta         ctermbg=NONE

    hi VertSplit                    cterm=reverse   ctermfg=NONE            ctermbg=NONE
    hi Visual                       cterm=NONE      ctermbg=blue            ctermfg=white
    hi VisualNOS                    cterm=bold      ctermbg=NONE            ctermfg=NONE

    hi WarningMsg                   cterm=bold      ctermfg=grey            ctermbg=red
    hi WildMenu                     cterm=NONE      ctermfg=black           ctermbg=darkyellow

" }}}
elseif &t_Co == 8 || &t_Co == 16
"{{{

    hi Comment                      cterm=NONE      ctermfg=3               ctermbg=NONE
    hi Constant                     cterm=NONE      ctermfg=red             ctermbg=NONE
    hi Cursornt                     cterm=NONE      ctermfg=NONE            ctermbg=NONE
    hi CursorLine                   cterm=underline ctermfg=NONE            ctermbg=NONE
    hi CursorColumn                 cterm=NONE      ctermfg=NONE            ctermbg=grey

    hi DoxygenArgumentWord          cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi Directory                    cterm=NONE      ctermfg=darkcyan        ctermbg=NONE
    hi DiffAdd                      cterm=NONE      ctermfg=NONE            ctermbg=lightblue
    hi DiffChange                   cterm=NONE      ctermfg=NONE            ctermbg=darkmagenta
    hi DiffDelete                   cterm=bold      ctermfg=lightblue       ctermbg=cyan
    hi DiffText                     cterm=bold      ctermfg=NONE            ctermbg=red
    hi DoxygenBOther                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenBriefLine             cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenBriefL                cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenComment               cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenCommentL              cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenParam                 cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenParamDirection        cterm=NONE      ctermfg=yellow          ctermbg=NONE
    hi DoxygenParamName             cterm=NONE      ctermfg=lightblue       ctermbg=NONE
    hi DoxygenSmallSpecial          cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenSpecialOneLineDesc    cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecialMultiLineDesc  cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi DoxygenSpecial               cterm=NONE      ctermfg=lightgreen      ctermbg=NONE
    hi DoxygenStart                 cterm=bold      ctermfg=yellow          ctermbg=NONE
    hi DoxygenStartL                cterm=NONE      ctermfg=lightgreen      ctermbg=NONE

    hi Error                        cterm=bold      ctermfg=grey            ctermbg=red
    hi ErrorMsg                     cterm=bold      ctermfg=grey            ctermbg=red

    hi Folded                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi FoldColumn                   cterm=NONE      ctermfg=darkyellow      ctermbg=NONE

    hi Identifier                   cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi Ignore                       cterm=NONE      ctermfg=blue            ctermbg=NONE
    hi IncSearch                    cterm=NONE      ctermfg=yellow          ctermbg=green

    hi LineNr                       cterm=NONE      ctermfg=darkyellow      ctermbg=NONE
    hi LineTooLong                  cterm=bold      ctermfg=white           ctermbg=blue

    hi MatchParen                   cterm=bold      ctermfg=darkmagenta     ctermbg=black
    hi MoreMsg                      cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi ModeMsg                      cterm=bold      ctermfg=green           ctermbg=NONE

    hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
    hi NonText                      cterm=bold      ctermfg=white           ctermbg=red
    hi Normal                       cterm=NONE      ctermfg=NONE            ctermbg=NONE

    hi Pmenu                        cterm=NONE      ctermfg=white           ctermbg=blue
    hi PmenuSbar                    cterm=NONE      ctermbg=white           ctermfg=white
    hi PmenuSel                     cterm=NONE      ctermfg=blue            ctermbg=white
    hi PmenuThumb                   cterm=NONE      ctermfg=black           ctermbg=black
    hi PreProc                      cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE

    hi Question                     cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Search                       cterm=NONE      ctermfg=grey            ctermbg=blue
    hi Special                      cterm=NONE      ctermfg=magenta         ctermbg=NONE
    hi SpecialKey                   cterm=NONE      ctermfg=darkgreen       ctermbg=NONE
    hi Statement                    cterm=NONE      ctermfg=cyan            ctermbg=NONE
    hi StatusLine                   cterm=NONE      ctermfg=blue            ctermbg=black
    hi StatusLineNC                 cterm=NONE      ctermfg=grey            ctermbg=NONE

    hi TabLine                      cterm=NONE      ctermfg=blue            ctermbg=black
    hi TabLineFill                  cterm=NONE      ctermfg=black           ctermbg=black
    hi TabLinePrev                  cterm=NONE      ctermfg=darkmagenta     ctermbg=black
    hi TabLineSel                   cterm=bold      ctermfg=green           ctermbg=black
    hi TabModded                    cterm=bold      ctermfg=white           ctermbg=red
    hi TabSeparator                 cterm=bold      ctermfg=blue            ctermbg=black
    hi Title                        cterm=NONE      ctermfg=darkmagenta     ctermbg=NONE
    hi Type                         cterm=NONE      ctermfg=green           ctermbg=NONE

    hi Underlined                   cterm=underline ctermfg=magenta         ctermbg=NONE

    hi VertSplit                    cterm=reverse   ctermfg=NONE            ctermbg=NONE
    hi Visual                       cterm=NONE      ctermbg=blue            ctermfg=white
    hi VisualNOS                    cterm=bold      ctermbg=NONE            ctermfg=NONE

    hi WarningMsg                   cterm=bold      ctermfg=grey            ctermbg=red
    hi WildMenu                     cterm=NONE      ctermfg=black           ctermbg=darkyellow

" }}}
endif

highlight Normal
        \ guifg=black
        \ guibg=#ECF0EC
highlight Comment
        \ cterm=bold
        \ ctermfg=red
        \ guifg=#4477AA
highlight Cursor
        \ gui=NONE
        \ guifg=#DDDDDD
        \ guibg=#FF0000
highlight CursorLine
        \ cterm=underline
        \ ctermbg=NONE
        \ ctermfg=NONE
        \ guibg=grey65
highlight CursorColumn
        \ cterm=reverse
        \ ctermbg=NONE
        \ gui=NONE
        \ guibg=grey65
highlight Constant
        \ cterm=underline
        \ ctermfg=red
        \ guifg=red
highlight Special
        \ cterm=bold
        \ ctermfg=magenta
        \ guifg=magenta
highlight Identifier
        \ cterm=NONE
        \ ctermfg=blue
        \ guifg=blue
highlight Statement
        \ cterm=NONE
        \ ctermfg=darkred
        \ gui=NONE
        \ guifg=Brown
highlight PreProc
        \ cterm=NONE
        \ ctermfg=darkmagenta
        \ guifg=Purple
highlight Type
        \ cterm=NONE
        \ ctermfg=blue
        \ gui=NONE
        \ guifg=blue
highlight NonText
        \ cterm=NONE
        \ ctermfg=white
        \ ctermbg=red
        \ guifg=white
        \ guibg=red
hi EndOfBuffer cterm=NONE ctermfg=blue ctermbg=NONE guifg=blue guibg=NONE
highlight Visual
        \ cterm=NONE
        \ ctermfg=yellow
        \ ctermbg=black
        \ gui=NONE
        \ guifg=Black
        \ guibg=Yellow
highlight Search
        \ cterm=reverse
        \ ctermfg=Black
        \ ctermbg=Cyan
        \ gui=NONE
        \ guifg=Black
        \ guibg=Cyan
highlight Tag
        \ cterm=bold
        \ ctermfg=darkGreen
        \ guifg=darkGreen
highlight Error
        \ cterm=reverse
        \ ctermfg=white
        \ ctermbg=red
        \ guifg=white
        \ guibg=red
highlight Todo
        \ cterm=standout
        \ ctermbg=Yellow
        \ ctermfg=Black
        \ guifg=blue
        \ guibg=Yellow
highlight Pmenu
        \ cterm=NONE
        \ ctermfg=white
        \ ctermbg=blue
        \ gui=NONE
        \ guifg=#88FFEE
        \ guibg=grey35
highlight PmenuSel
        \ cterm=NONE
        \ ctermfg=blue
        \ ctermbg=white
        \ gui=NONE
        \ guifg=blue
        \ guibg=#88FFEE
highlight PmenuSbar
        \ cterm=NONE
        \ ctermbg=white
        \ ctermfg=white
        \ guifg=#448877
        \ guibg=#448877
highlight PmenuThumb
        \ cterm=NONE
        \ ctermfg=black
        \ ctermbg=black
        \ guifg=black
        \ guibg=black
highlight Folded
        \ ctermfg=blue
        \ ctermbg=NONE
        \ guifg=#2244FF
        \ guibg=NONE
highlight FoldColumn
        \ ctermfg=blue
        \ ctermbg=NONE
        \ guifg=brown
        \ guibg=NONE
highlight TabLine
        \ cterm=underline
        \ ctermfg=black
        \ ctermbg=grey
        \ gui=underline
        \ guifg=black
        \ guibg=grey65
highlight TabLineSel
        \ cterm=bold
        \ ctermfg=white
        \ ctermbg=red
        \ gui=bold
        \ guifg=#7744FF
        \ guibg=#ECF0EC
highlight TabLinePrev
        \ cterm=underline
        \ ctermfg=black
        \ ctermbg=darkmagenta
        \ gui=underline
        \ guifg=grey65
        \ guibg=grey35
highlight TabModded
        \ cterm=bold
        \ ctermfg=white
        \ ctermbg=red
        \ gui=bold
        \ guifg=white
        \ guibg=red
highlight TabLineFill
        \ cterm=NONE
        \ ctermfg=grey
        \ ctermbg=grey
        \ gui=NONE
        \ guifg=black
        \ guibg=black
highlight TabSeparator
        \ cterm=bold
        \ ctermfg=blue
        \ ctermbg=black
        \ gui=bold
        \ guifg=#0000FF
        \ guibg=black
highlight StatusLine
        \ cterm=NONE
        \ ctermfg=blue
        \ ctermbg=black
        \ gui=NONE
        \ guifg=skyblue
        \ guibg=black
highlight StatusLineNC
        \ cterm=NONE
        \ ctermfg=grey
        \ ctermbg=NONE
        \ gui=NONE
        \ guifg=grey
        \ guibg=NONE
highlight MatchParen
        \ cterm=bold
        \ ctermfg=darkmagenta
        \ ctermbg=black
        \ gui=bold
        \ guifg=white
        \ guibg=blue
highlight LineTooLong
        \ cterm=bold
        \ ctermfg=white
        \ ctermbg=blue
        \ guifg=green
        \ guibg=black
highlight DoxygenComment
        \ cterm=NONE
        \ ctermfg=darkgrey
        \ ctermbg=NONE
        \ gui=NONE
        \ guifg=grey15
        \ guibg=NONE
highlight DoxygenCommentL
        \ cterm=NONE
        \ ctermfg=darkgrey
        \ ctermbg=NONE
        \ gui=NONE
        \ guifg=grey15
        \ guibg=NONE
highlight DoxygenStart
        \ cterm=NONE
        \ ctermfg=darkgrey
        \ ctermbg=NONE
        \ guifg=grey15
        \ guibg=bg
        \ gui=NONE
highlight DoxygenStartL
        \ cterm=NONE
        \ ctermfg=darkgrey
        \ ctermbg=NONE
        \ guifg=grey15
        \ guibg=bg
        \ gui=NONE
highlight DoxygenBriefLine
        \ cterm=NONE
        \ ctermfg=darkgreen
        \ ctermbg=NONE
        \ guifg=#00aa00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenBriefL
        \ cterm=NONE
        \ ctermfg=darkgreen
        \ ctermbg=NONE
        \ guifg=#00aa00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenCommentL
        \ cterm=NONE
        \ ctermfg=darkgreen
        \ ctermbg=NONE
        \ guifg=#00aa00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenSpecialOneLineDesc
        \ cterm=NONE
        \ ctermfg=darkgreen
        \ ctermbg=NONE
        \ guifg=#00aa00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenSpecialMultiLineDesc
        \ cterm=NONE
        \ ctermfg=darkgreen
        \ ctermbg=NONE
        \ guifg=#00aa00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenSpecial
        \ cterm=NONE
        \ ctermfg=red
        \ ctermbg=NONE
        \ guifg=red
        \ guibg=bg
        \ gui=NONE
highlight DoxygenSmallSpecial
        \ cterm=NONE
        \ ctermfg=lightgreen
        \ ctermbg=NONE
        \ guifg=#00ff00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenParam
        \ cterm=NONE
        \ ctermfg=red
        \ ctermbg=NONE
        \ guifg=red
        \ guibg=bg
        \ gui=NONE
highlight DoxygenBOther
        \ cterm=NONE
        \ ctermfg=red
        \ ctermbg=NONE
        \ guifg=red
        \ guibg=bg
        \ gui=NONE
highlight DoxygenParamName
        \ cterm=NONE
        \ ctermfg=brown
        \ ctermbg=NONE
        \ guifg=brown
        \ guibg=bg
        \ gui=NONE
highlight DoxygenParamDirection
        \ cterm=NONE
        \ ctermfg=darkyellow
        \ ctermbg=NONE
        \ guifg=#cccc00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenParamDirection
        \ cterm=NONE
        \ ctermfg=yellow
        \ ctermbg=NONE
        \ guifg=#ffff00
        \ guibg=bg
        \ gui=NONE
highlight DoxygenArgumentWord
        \ cterm=NONE
        \ ctermfg=lightblue
        \ ctermbg=NONE
        \ guifg=lightblue
        \ guibg=bg
        \ gui=NONE

" highlight! link MoreMsg         Comment
" highlight! link ErrorMsg        Visual
" highlight! link WarningMsg      ErrorMsg
" highlight! link Question        Comment
" highlight  link String          Constant
" highlight  link Character       Constant
" highlight  link Number          Constant
" highlight  link Boolean         Constant
" highlight  link Float           Number
" highlight  link Function        Identifier
" highlight  link Conditional     Statement
" highlight  link Repeat          Statement
" highlight  link Label           Statement
" highlight  link Operator        Statement
" highlight  link Keyword         Statement
" highlight  link Exception       Statement
" highlight  link Include         PreProc
" highlight  link Define          PreProc
" highlight  link Macro           PreProc
" highlight  link PreCondit       PreProc
" highlight  link StorageClass    Type
" highlight  link Structure       Type
" highlight  link Typedef         Type
" highlight  link SpecialChar     Special
" highlight  link Delimiter       Special
" highlight  link SpecialComment  Special
" highlight  link Debug           Special
