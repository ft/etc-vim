hi hitpoints ctermfg=red
hi name      ctermfg=green
hi advance   ctermfg=magenta
hi attack    ctermfg=cyan
hi gold      ctermfg=yellow
hi exp       ctermfg=magenta

syn match hitpoints /\(max_\|\)hitpoints=.*/
syn match name      /\<\(id\|user_description\)=.*/
syn match gold      /\<gold=.*/
syn match advance   /\<\(advanceto\|advancesto\|advances_to\)=.*/
syn match gold      /\[attack\]/
syn match exp       /\(max_\|\)experience=.*/

nmap b :s/experience=".*"$/experience="500000"/<CR>
nmap B :s/max_experience=".*"$/max_experience="1"/<CR>/experience=".*"$<CR>
nmap m :s/hitpoints=".*"$/hitpoints="820"/<CR>
