""" vim:ft=vim:fdm=marker

    set notitle             " don't set screen hardstatus here
    set filetype=mail       " use mailtype highlighting
    set textwidth=70        " max. linelength 70 characters
    set spell               " switch on spell checking
    set cursorline          " highlight the current line
    set nobackup            " no backups
    set nolist              " listchars are also useless in emails
    set laststatus=0        " no need for statusline or tabline here
    set showtabline=0

""" insert zblog posting template
    nnoremap <F10> :%<CR>:read ~/etc/zblog/zblogtemplate<CR>:$-1<CR>i

""" some special keys
    vmap ,kpq :s/^> *[a-zA-Z]*>/> >/<C-M>
    map  ,kqs G?^> -- $<CR>d}
    vmap ,rb  :s/^\(> *\)\+$//<C-M>
    vmap ,rl  :s/^\(> *\)\+\n//<C-M>
    map  ,rb  :%s/^\(> *\)\+$//<C-M>
    map  ,rl  :%s/^\(> *\)\+\n//<C-M>

""" abbreviations
    abbreviate _ee Regards, Frank
    abbreviate _eg Gru�, Frank
    abbreviate _sn [snip]<CR>[snap]

""" get me my [snip] [snap] region; and no spellchecking in signatures
    syntax region SnipSnap  start="^\[snip\]$" end="^\[snap\]$" contains=@NoSpell
    syntax region mailSig   start="^-- $"      end="\%$"        contains=@NoSpell

""" changed highlighting for mails; esp. change colors of different quoting levels
    hi SnipSnap        ctermfg=darkcyan
    hi mailSig         ctermfg=darkred
    hi mailSignature   ctermfg=darkred
    hi mailHeader      ctermfg=darkyellow
    hi mailHeaderKey   ctermfg=darkmagenta
    hi mailHeaderEmail ctermfg=darkgreen
    hi mailEmail       ctermfg=darkmagenta
    hi mailURL         ctermfg=darkred
    hi mailSubject     ctermfg=lightblue
    hi mailQuoted1     ctermfg=darkgreen
    hi mailQuoted2     ctermfg=darkyellow
    hi mailQuoted3     ctermfg=darkcyan
    hi mailQuoted4     ctermfg=darkred
    hi mailQuoted5     ctermfg=darkmagenta
    hi mailQuoted6     ctermfg=darkblue

""" Go to the first blank line; useful if 'edit_headers' is set in mutt.
    exec "normal gg"
    call search("^$")
