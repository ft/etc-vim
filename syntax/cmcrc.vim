" Copyright 2009,2010 cmc workers, All rights reserved.
"
" Redistribution and use in source and binary forms, with or without
" modification, are permitted provided that the following conditions
" are met:
"
" 1. Redistributions of source code must retain the above copyright
"    notice, this list of conditions and the following disclaimer.
" 2. Redistributions in binary form must reproduce the above copyright
"    notice, this list of conditions and the following disclaimer in the
"    documentation and/or other materials provided with the distribution.
"
" THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
" INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
" AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
" THE AUTHOR OR CONTRIBUTORS OF THE PROJECT BE LIABLE FOR ANY DIRECT,
" INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
" (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
" SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
" STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
" IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
" POSSIBILITY OF SUCH DAMAGE.

" vim syntax file for cmc's configuration file

if exists("b:current_syntax")
  finish
endif

syn keyword cmcSection              core keys terminal
syn keyword cmcSubSection           key
syn keyword cmcOption               autoconnect autoreconnect autolaunch command defaultview id keymaps omitdefkeys verbose
                                \   term_pattern ostype_pattern display_pattern
syn keyword cmcConstant             true false
syn keyword cmcTodo                 contained TODO FIXME

syn cluster cmcCommentGroup         contains=cmcTodo

syn match   cmcSpecial              display contained "\\\(x\x\+\|\o\{1,3}\|.\|$\)"
syn region  cmcString               start=+L\="+ skip=+\\\\\|\\"+ end=+"+ contains=cmcSpecial
syn region  cmcString               start=+L\='+ skip=+\\\\\|\\'+ end=+'+

syn match   cmcNumber               display contained "\d\+"

syn region  cmcCommentL             start="\(//\|#\)" skip="\\$" end="$" keepend contains=@cmcCommentGroup,@Spell
syn region  cmcComment              matchgroup=cmcCommentStart start="/\*" end="\*/" contains=@cmcCommentGroup,cmcCommentStartError,@Spell
syn match   cmcCommentError         display "\*/"
syn match   cmcCommentStartError    display "/\*"me=e-1 contained

hi def link cmcConstant             Constant
hi def link cmcComment              Comment
hi def link cmcCommentL             cmcComment
hi def link cmcCommentStart         cmcComment
hi def link cmcCommentError         cmcError
hi def link cmcCommentStartError    cmcError
hi def link cmcError                Error
hi def link cmcNumber               Number
hi def link cmcSpecial              SpecialChar
hi def link cmcSection              Statement
hi def link cmcSubSection           PreProc
hi def link cmcOption               Type
hi def link cmcString               String
hi def link cmcTodo                 Todo

let b:cmcurrent_syntax = "cmcrc"
