" Vim syntax file
" Language:     c*mus configuration
" Maintainer:   Frank Terbeck <frank.terbeck@rwth-aachen.de>
" Last Change:  Jan 28 2006

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" comments
syn match     cmusComment     "#.*$"

" keys
syn match     cmusKeys        "\s\^\?[][a-zA-Z0-9\*\-\.+,_=}{)(][[:space:]\n]"
syn match     cmusKeys        "\sF[0-9]*\s"
syn match     cmusKeys        "\s\(up\|down\|home\|left\|right\|space\|end\|page_up\|page_down\|tab\|enter\|delete\|backspace\)\s"

" keytables
syn match     cmusKeytabs     "\s\(browser\|common\|filters\|library\|playlist\|queue\)\s"me=e-1,ms=s+1

" variables
syn keyword   cmusVar         continue repeat shuffle play_library play_sorted show_remaining_time status_display_program
syn keyword   cmusVar         lib_sort pl_sort output_plugin aaa_mode buffer_seconds show_hidden auto_reshuffle
syn keyword   cmusVar         altformat_current altformat_playlist altformat_title altformat_trackwin id3_default_charset
syn keyword   cmusVar         format_current format_playlist format_title format_trackwin confirm_run default_view
syn match     cmusVar         "dsp.ao.\(buffer_size\|driver\|wav_counter\|wav_dir\)"
syn match     cmusVar         "\(mixer.alsa.device\|dsp.oss.device\|mixer.oss.channel\|mixer.oss.device\)"
syn match     cmusVar         "\(mixer.alsa.channel\|dsp.alsa.device\)"
syn match     cmusVar         "color_[a-zA-Z_\.]*"

" set var=cmusValue
syn match     cmusValue       "=.*$"hs=s+1

" operator
syn match     cmusOperator    ":"

" commands
syn keyword   cmusCommands    set bind source add cd clear colorscheme echo factivate filter invert load mark quit refresh
syn keyword   cmusCommands    run save seek set showbind source toggle unbind unmark view vol
syn match     cmusCommands    "\(browser-up\|player-next\|player-pause\|player-play\|player-prev\|player-stop\)"
syn match     cmusCommands    "\(search-next\|search-prev\|win-activate\|win-add-l\|win-add-p\|win-add-Q\|win-add-q\)"
syn match     cmusCommands    "\(win-mv-after\|win-mv-before\|win-next\|win-page-down\|win-page-up\|win-remove\)"
syn match     cmusCommands    "\(win-sel-cur\|win-toggle\|win-top\|win-up.*\|win-bottom\|win-down\)"
syn match     cmusCommands    "^\s*shuffle"

" command flags
syn match     cmusFlags       "\s-[Qlpqf]\s"

" playlist filters
syn region    cmusFSet        start="^\s*fset" end="="me=e-1  contains=cmusFilter keepend
syn match     cmusFilter      "[a-zA-Z0-9_\-]*="me=e-1        contained contains=cmusValue

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_cmus_syntax_inits")
  if version < 508
    let did_cmus_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink  cmusComment     Comment
  HiLink  cmusVar         Type
  HiLink  cmusCommands    Statement
  HiLink  cmusFSet        Statement
  HiLink  cmusValue       String
  HiLink  cmusBoundComm   String
  HiLink  cmusOperator    Operator
  HiLink  cmusKeytabs     Type
  HiLink  cmusFilter      Type
  HiLink  cmusKeys        String
  HiLink  cmusFlags       String

  delcommand HiLink
endif

let b:current_syntax = "cmus"
