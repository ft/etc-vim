" Vim syntax file
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" keys
syn match     irclogDate        "^[0-9][0-9]:[0-9][0-9]"
syn match     irclogNick        " <[a-zA-Z0-9@]*>" contains=irclogOP
syn match     irclogText        "  .*$"
syn match     irclogLog         "^--- .*$"
syn match     irclogClMsg       "^-\!- .*$"
syn match     irclogOp          "@"
syn match     irclogAction      "  \* .*"

if version >= 508 || !exists("did_cmus_syntax_inits")
  if version < 508
    let did_cmus_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink  irclogText      Comment
  HiLink  irclogNick      Statement
  HiLink  irclogDate      Delimiter
  HiLink  irclogLog       Type
  HiLink  irclogClMsg     String
  HiLink  irclogOp        Type
  HiLink  irclogAction    String

  delcommand HiLink
endif

let b:current_syntax = "irclog"
